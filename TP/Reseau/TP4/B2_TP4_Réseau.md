# B2_TP4_Réseau

## I. Dumb switch

### 3. Setup topologie 1

#### 🌞 Commençons simple

```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=4.472 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=6.188 ms
```

```
PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=9.511 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=10.481 ms

```

## II. VLAN

### 3. Setup topologie 2

#### 🌞 Adressage

pc1

```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=5.933 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=3.978 ms
```

```
PC1> ping 10.1.1.3

84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=10.249 ms
84 bytes from 10.1.1.3 icmp_seq=2 ttl=64 time=4.677 ms
```

pc2

```
PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=11.074 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=8.620 ms
```

```
PC2> ping 10.1.1.3

84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=8.819 ms
84 bytes from 10.1.1.3 icmp_seq=2 ttl=64 time=6.607 ms
```

pc3

```
PC3> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=10.642 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=7.757 ms
```

```
PC3> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=8.257 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=10.433 ms
```

#### 🌞 Configuration des VLANs

```
Switch#sh vlan br

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi0/3, Gi1/0, Gi1/1, Gi1/2
                                                Gi1/3, Gi2/0, Gi2/1, Gi2/2
                                                Gi2/3, Gi3/0, Gi3/1, Gi3/2
                                                Gi3/3
10   admins                           active    Gi0/0, Gi0/1
20   guests                           active    Gi0/2

```

#### 🌞 Vérif

pc1

```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=6.972 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=3.763 ms
```

```
PC1> ping 10.1.1.3

host (10.1.1.3) not reachable
```

pc2

```
PC2> pin 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=9.364 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=10.855 ms
```

```
PC2> pin 10.1.1.3

host (10.1.1.3) not reachable
```

pc3

```
PC3> ping 10.1.1.1

host (10.1.1.1) not reachable
```

```
PC3> ping 10.1.1.2

host (10.1.1.2) not reachable
```

## III. Routing

### 3. Setup topologie 3

#### 🌞 Adressage

```
admin1> show ip

NAME        : admin1[1]
IP/MASK     : 10.2.2.1/24

<---------------------------------------------------------------->

PC1> show ip

NAME        : PC1[1]
IP/MASK     : 10.1.1.1/24

<---------------------------------------------------------------->

PC2> show ip

NAME        : PC2[1]
IP/MASK     : 10.1.1.2/24

```

#### 🌞 Configuration des VLANs

- Déclaration des VLANs sur le switch `sw1`
- Ajout des ports du switches dans le bon VLAN

```
Switch#show vlan

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi1/0, Gi1/1, Gi1/2, Gi1/3
                                                Gi2/0, Gi2/1, Gi2/2, Gi2/3
                                                Gi3/0, Gi3/1, Gi3/2, Gi3/3
11   clients                          active    Gi0/0, Gi0/1
12   admins                           active    Gi0/2
13   servers                          active    Gi0/3
```
- Il faudra ajouter le port qui pointe vers le *routeur* comme un *trunk* : c'est un port entre deux équipements réseau (un *switch* et un *routeur*)

```
Switch(config)#interface gigabitEthernet1/0
Switch(config-if)#switchport trunk encapsulation dot1q
Switch(config-if)#switchport mode trunk
Switch(config-if)#switchport trunk allowed vlan add 11,12,13
Switch(config-if)#exit
Switch#show interfaces trunk

Port        Mode             Encapsulation  Status        Native vlan
Gi1/0       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Gi1/0       1-4094

Port        Vlans allowed and active in management domain
Gi1/0       1,11-13

Port        Vlans in spanning tree forwarding state and not pruned
Gi1/0       none
Switch#

```

#### 🌞 Config du routeur

```
VLAN 11 = 

R1(config)#interface fastEthernet 0/0.11
R1(config-subif)#encapsulation dot1Q 11
R1(config-subif)#ip addr 10.1.1.254 255.255.255.0
R1(config-subif)#exit

VLAN 12 =

R1(config)#interface fastEthernet 0/0.12
R1(config-subif)#encapsulation dot1Q 12
R1(config-subif)#ip addr 10.2.2.254 255.255.255.0
R1(config-subif)#exit

VLAN 13 =
R1(config)#interface fastEthernet 0/0.13
R1(config-subif)#encapsulation dot1Q 13
R1(config-subif)#ip addr 10.3.3.254 255.255.255.0
R1(config-subif)#exit
```

#### 🌞 Vérif

- Tout le monde doit pouvoir ping le routeur sur l'IP qui est dans son réseau

```
PC1> ping 10.1.1.254

84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=23.962 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=18.660 ms

<---------------------------------------------------------------->
PC2> ping 10.1.1.254

84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=8.918 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=21.382 ms

<---------------------------------------------------------------->

adm1> ping 10.2.2.254

84 bytes from 10.2.2.254 icmp_seq=1 ttl=255 time=14.439 ms
<----------------------------------------------------------------> 
[greg@web1 ~]$ ping 10.3.3.254

PING 10.3.3.254 (10.3.3.254) 56(84) bytes of data.
64 bytes from 10.3.3.254: icmp_seq=1 ttl=255 time=37.7 ms
64 bytes from 10.3.3.254: icmp_seq=2 ttl=255 time=16.10 ms
```
- En ajoutant une route vers les réseaux, ils peuvent se ping entre eux
- Ajoutez une route par défaut sur les VPCS
```

PC1> ip 10.1.1.1/24  10.1.1.254
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0 gateway 10.1.1.254


PC2> ip 10.1.1.2/24 10.1.1.254
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0 gateway 10.1.1.254

adm1> ip 10.2.2.1/24 10.2.2.254
Checking for duplicate address...
adm1 : 10.2.2.1 255.255.255.0 gateway 10.2.2.254

```
- Ajoutez une route par défaut sur la machine virtuelle
```
Pour la machine virtuel (web) , il faut aller dans /etc/sysconfig/network avec vi ou nano pour pouvoir lui renseigner la gateway qui est 10.3.3.254

On peut voir qu'il ping bien tous les reseaux (voir si dessous)
```
- Testez des `ping` entre les réseaux
```
Ping de PC1 vers admin: 

PC1> ping 10.2.2.1

84 bytes from 10.2.2.1 icmp_seq=1 ttl=63 time=52.929 ms
84 bytes from 10.2.2.1 icmp_seq=2 ttl=63 time=38.082 ms
PC1>

<----------------------------------------------------------------> 

Ping de la machine virtuel vers pc2:

[greg@web1 ~]$ ping 10.1.1.2
PING 10.1.1.2 (10.1.1.2) 56(84) bytes of data.
64 bytes from 10.1.1.2: icmp_seq=1 ttl=63 time=38.9 ms
64 bytes from 10.1.1.2: icmp_seq=2 ttl=63 time=53.4 ms

<----------------------------------------------------------------> 

Ping de la machine virtuel vers admin:

[greg@web1 ~]$ ping 10.2.2.1
PING 10.2.2.1 (10.2.2.1) 56(84) bytes of data.
64 bytes from 10.2.2.1: icmp_seq=1 ttl=63 time=62.3 ms

<---------------------------------------------------------------->

Ping pc2 vers admin :

PC2> ping 10.2.2.1

84 bytes from 10.2.2.1 icmp_seq=1 ttl=63 time=71.238 ms
84 bytes from 10.2.2.1 icmp_seq=2 ttl=63 time=34.677 ms

```

## IV. NAT

### 3. Setup topologie 4

#### 🌞 Ajoutez le noeud Cloud à la topo

- Branchez à `eth1` côté Cloud

```
Le cloud est bien branché en eth1 du coté cloud et sur le swictch a la borne GI1/1
```

- Côté routeur, il faudra récupérer un IP en DHCP

```

R1#conf t
R1(config)#interface fastEthernet1/0
R1(config-if)#ip address dhcp
R1(config-if)#no shutdown
R1(config-if)#exit
R1(config)#exit

R1#show ip interface brief
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            unassigned      YES NVRAM  administratively down down
FastEthernet0/0.11         10.1.1.254      YES NVRAM  administratively down down
FastEthernet0/0.12         10.2.2.254      YES NVRAM  administratively down down
FastEthernet0/0.13         10.3.3.254      YES NVRAM  administratively down down
FastEthernet1/0            unassigned      YES DHCP   up                    up  
FastEthernet2/0            unassigned      YES NVRAM  administratively down down
FastEthernet3/0            unassigned      YES NVRAM  administratively down down

```
- Vous devriez pouvoir `ping 1.1.1.1`
  
```
Vu que mon interface F1/0 à bien une adresse ip, je peux donc ping 1.1.1.1


R1#ping 1.1.1.1

Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 1.1.1.1, timeout is 2 seconds:
.!!!!
Success rate is 80 percent (4/5), round-trip min/avg/max = 14/24/37 ms
R1#

```

#### 🌞 Configurez le NAT

```
R1(config)#interface fastEthernet1/0
R1(config-if)#ip nat inside
R1(config-if)#exit
R1(config)#interface fastEthernet2/0
R1(config-if)#ip nat inside
R1(config-if)#exit
R1(config)#access-list 1 permit any
R1(config)#ip nat inside source list 1 interface fastEthernet 0/0 overload
```

#### 🌞 Test

Pour mettre un dns sur les vpcs il faut faire un ip dns 1.1.1.1 sur chaque machine:

```
PC1> ip dns 1.1.1.1
PC2> ip dns 1.1.1.1
admin> ip dns 1.1.1.1
```

et pour la vm il faut aller directement dans  /etc/resolv.conf (ex: avec vim):

```
search auvence.co servers.tp4
nameserver 8.8.8.8
nameserver 1.1.1.1
```

- vérifiez un `ping` vers un nom de domaine

```
PC1> ping google.com
google.com resolved to 142.250.179.110

84 bytes from 142.250.179.110 icmp_seq=1 ttl=113 time=61.948 ms
84 bytes from 142.250.179.110 icmp_seq=2 ttl=113 time=56.299 ms

PC2> ping google.com
google.com resolved to 142.250.179.110

84 bytes from 142.250.179.110 icmp_seq=1 ttl=113 time=37.272 ms
84 bytes from 142.250.179.110 icmp_seq=2 ttl=113 time=44.963 ms

adm1> ping google.com
google.com resolved to 142.250.179.110

84 bytes from 142.250.179.110 icmp_seq=1 ttl=113 time=41.698 ms
84 bytes from 142.250.179.110 icmp_seq=2 ttl=113 time=38.295 ms

```

## V. Add a building

### 3. Setup topologie 5

#### 🌞  Mettre en place un serveur DHCP dans le nouveau bâtiment

```
[greg@dhcp ~]$ cat /etc/dhcp/dhcpd.conf

# this DHCP server to be declared valid
authoritative;
# specify network address and subnetmask
subnet 10.1.1.0 netmask 255.255.255.0 {
    option subnet-mask 255.255.255.0;
    # specify the range of lease IP address
    range dynamic-bootp 10.1.1.4 10.1.1.252;
    # specify broadcast address
    option broadcast-address 10.3.1.63;
    # specify gateway
    option routers 10.1.1.254;
    # specifiy dns
    option domain-name-servers 1.1.1.1;
}
```

#### 🌞  Vérification

- Un client récupère une IP en DHCP

```
PC4> ip dhcp
DDORA IP 10.1.1.4/24 GW 10.1.1.254

PC3> show ip

NAME        : PC4[1]
IP/MASK     : 10.1.1.4/24
GATEWAY     : 10.1.1.254
DNS         : 1.1.1.1
DHCP SERVER : 10.1.1.253
DHCP LEASE  : 43131, 43200/21600/37800
MAC         : 00:40:68:70:58:04
LPORT       : 20064
RHOST:PORT  : 127.0.0.1:20065
MTU         : 1500
```

- Il peut ping le serveur Web
```
PC4> ping 10.3.3.1

10.3.3.1 icmp_seq=1 timeout
84 bytes from 10.3.3.1 icmp_seq=2 ttl=63 time=36.962 ms
84 bytes from 10.3.3.1 icmp_seq=3 ttl=63 time=41.167 ms
84 bytes from 10.3.3.1 icmp_seq=4 ttl=63 time=86.357 ms
```
- Il peut ping 8.8.8.8

```

PC4> ping 8.8.8.8

8.8.8.8 icmp_seq=1 timeout
84 bytes from 8.8.8.8 icmp_seq=2 ttl=114 time=72.571 ms
84 bytes from 8.8.8.8 icmp_seq=3 ttl=114 time=51.321 ms
```

- Il peut ping google.com


```
PC4> ping google.com
google.com resolved to 216.58.204.110

84 bytes from 216.58.204.110 icmp_seq=1 ttl=113 time=60.647 ms
84 bytes from 216.58.204.110 icmp_seq=2 ttl=113 time=66.853 ms
```
