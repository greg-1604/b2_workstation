# B2_TP3_Réseau

## 0. Prérequis

### I. (mini)Architecture réseau

#### 1. Adressage

##### 🌞 Vous me rendrez un 🗃️ tableau des réseaux 🗃️ qui rend compte des adresses choisies, sous la forme:

| Nom du réseau | Adresse du réseau | Masque        | Nombre de clients possibles | Adresse passerelle | Adresse broadcast   |
|---------------|-------------------|---------------|-----------------------------|--------------------|---------------------|
| `server1`     | `10.3.1.0`        | `255.255.255.128` | `126`                           | `10.3.1.126`         | `10.3.1.127`          |
| `client1`     | `10.3.1.128`        | `255.255.255.192` | `62`                           | `10.3.1.190`         | `10.3.1.191`          |
| `server2`     | `10.3.1.192`        | `255.255.255.240` | `14`                           | `10.3.1.206`         | `10.3.1.207`                                                                                   |

#### 2. Routeur

##### 🌞 Vous pouvez d'ores-et-déjà créer le routeur. Pour celui-ci, vous me prouverez que:

- il a bien une IP dans les 3 réseaux, l'IP que vous avez choisie comme IP de passerelle

```
[greg@router ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:4f:97:53 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 86104sec preferred_lft 86104sec
    inet6 fe80::a00:27ff:fe4f:9753/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:72:0d:af brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.190/26 brd 10.3.1.191 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe72:daf/64 scope link
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e1:52:b5 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.126/25 brd 10.3.1.127 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fee1:52b5/64 scope link
       valid_lft forever preferred_lft forever
5: enp0s10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:b6:51:2f brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.206/28 brd 10.3.1.207 scope global noprefixroute enp0s10
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feb6:512f/64 scope link
       valid_lft forever preferred_lft forever
```

- il a un accès internet

```
[greg@router ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=19.6 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=20.9 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1004ms
rtt min/avg/max/mdev = 19.644/20.292/20.940/0.648 ms
```

- il a de la résolution de noms

```
[greg@router ~]$ ping ynov.com
PING ynov.com (92.243.16.143) 56(84) bytes of data.
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=53 time=17.8 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=53 time=18.9 ms
^C
--- ynov.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 17.796/18.369/18.943/0.589 ms
```

- il porte le nom router.tp3*

```
[greg@router ~]$ cat /etc/hostname
router.tp3
```

- n'oubliez pas d'activer le routage sur la machine

```
[greg@router ~]$ sudo firewall-cmd --add-masquerade --zone=public
[sudo] password for greg:
success
[greg@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```

### II. Services d'infra

#### 1. Serveur DHCP

##### 🌞 Mettre en place une machine qui fera office de serveur DHCP dans le réseau client1. Elle devra:

- porter le nom dhcp.client1.tp3

```
[greg@dhcp ~]$ cat /etc/hostname
dhcp.client.tp3
```

- donner une IP aux machines clients qui le demande
- leur donner l'adresse de leur passerelle
- leur donner l'adresse d'un DNS utilisable

```
[greg@dhcp ~]$ sudo !!
sudo cat /etc/dhcp/dhcpd.conf
[sudo] password for greg:
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
default-lease-time 900;
max-lease-time 10800;
ddns-update-style none;
authoritative;
subnet 10.3.1.128 netmask 255.255.255.192 {
        range 10.3.1.131 10.3.1.189;
        option routers 10.3.1.190;
        option subnet-mask 255.255.255.192;
        option domain-name-servers 8.8.8.8;
}
```

##### 🌞 Mettre en place un client dans le réseau client1

```
  GNU nano 2.9.8        /etc/sysconfig/network-scripts/ifcfg-enp0s8                  

BOOTPROTO=dhcp
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
```

```
[greg@marcel ~]$ cat /etc/hostname
marcel.client1.tp3
```

Comme la carte NAT est désactivé, si on peut ping (8.8.8.8 par exemple), c'est que la passerelle est bien configuré:
```
[greg@marcel ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=24.2 ms
^C
--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 24.188/24.188/24.188/0.000 ms
```

##### 🌞 Depuis marcel.client1.tp3

- Prouver qu'il a un accès internet + résolution de noms, avec des infos récupérées par votre DHCP

```
[greg@marcel ~]$ ping ynov.com
PING ynov.com (92.243.16.143) 56(84) bytes of data.
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=50 time=22.8 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=50 time=23.10 ms
^C
--- ynov.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 22.833/23.397/23.962/0.584 ms
```

- A l'aide de la commande traceroute, prouver que marcel.client1.tp3 passe par router.tp3 pour sortir de son réseau

```
[greg@marcel ~]$ traceroute google.com
traceroute to google.com (172.217.19.238), 30 hops max, 60 byte packets
 1  _gateway (10.3.1.190)  6.267 ms  5.796 ms  5.423 ms
 2  10.0.2.2 (10.0.2.2)  4.645 ms  4.343 ms  4.046 ms
```

#### 3. Serveur DNS

##### 🌞 Mettre en place une machine qui fera office de serveur DNS

- Dans le réseau server1
```
Mon server DNS1 est bien dans le reseau de mon serveur 1

[greg@dns1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:4f:97:53 brd ff:ff:ff:ff:ff:ff
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:0e:cc:af brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.2/25 brd 10.3.1.127 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe0e:ccaf/64 scope link
       valid_lft forever preferred_lft forever
[greg@dns1 ~]$

```
- De son p'tit nom dns1.server1.tp3
```
[greg@dns1 ~]$ hostname
dns1.server1.tp3
[greg@dns1 ~]$
```

- Il faudra lui ajouter un serveur DNS public connu, afin qu'il soit capable de résoudre des noms publics comme google.com
  conf classique avec le fichier /etc/resolv.conf ou les fichiers de conf d'interface
```
  La commande dig marche bien 

  [greg@dns1 ~]$ sudo dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 52156
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               7318    IN      A       92.243.16.143

;; Query time: 20 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Thu Sep 30 15:28:23 CEST 2021
;; MSG SIZE  rcvd: 53
  ```

- Comme pour le DHCP, on part sur "rocky linux dns server" on Google pour les détails de l'install du serveur DNS

- Le paquet que vous allez installer devrait s'appeler bind : c'est le nom du serveur DNS le plus utilisé au monde
```
dnf -y install bind bind-utils
```


- Il y aura plusieurs fichiers de conf :

  un fichier de conf principal named.conf

  des fichiers de zone "forward"

permet d'indiquer une correspondance nom -> IP
un fichier par zone forward


vous ne mettrez pas en place de zones reverse, uniquement les forward
on ne met PAS les clients dans les fichiers de zone car leurs adresses IP peuvent changer (ils les récupèrent à l'aide du DHCP)

donc votre DNS gérera deux zones : server1.tp3 et server2.tp3

les réseaux où les IPs sont définies de façon statique !
```
acl "allowed" {
        10.3.1.2/25;
};

options {
        listen-on port 53 { 127.0.0.1; 10.3.1.2; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";
        allow-query     { 10.3.1.1; 10.3.1.128; 10.3.1.192; };

        recursion yes;
        dnssec-enable yes;
        dnssec-validation yes;
        managed-keys-directory "/var/named/dynamic";
        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";
        include "/etc/crypto-policies/back-ends/bind.config";
};
logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "forward.server1.tp3" IN {
        type master;                           # type of zone
        file "/var/named/forward.server1.tp3"; # location of forward zone file
        allow-update { none; };
};

zone    "forward.dhcp.client" IN {
        type master;
        file "/var/named/forward.dhcp.client"; # location of forward zone file
        allow-update { none; };
};
zone "forward.server2.tp3" IN {
        type master;                           # type of zone
        file "/var/named/forward.server2.tp3"; # location of forward zone file
        allow-update { none; };
};
```
```
[greg@dns1 ~]$ sudo  systemctl status named
● named.service - Berkeley Internet Name Domain (DNS)
   Loaded: loaded (/usr/lib/systemd/system/named.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-10-05 15:15:38 CEST; 11s ago
  Process: 2541 ExecStart=/usr/sbin/named -u named -c ${NAMEDCONF} $OPTIONS (code=exited, status=0/SUCCESS)
  Process: 2538 ExecStartPre=/bin/bash -c if [ ! "$DISABLE_ZONE_CHECKING" == "yes" ]; then /usr/sbin/named-checkconf -z "$NAMEDCONF"; else echo "Checking of zone files is d>
 Main PID: 2543 (named)
    Tasks: 4 (limit: 11398)
   Memory: 58.5M
   CGroup: /system.slice/named.service
           └─2543 /usr/sbin/named -u named -c /etc/named.conf
```
##### 🌞 Tester le DNS depuis marcel.client1.tp3

- Définissez manuellement l'utilisation de votre serveur DNS
essayez une résolution de nom avec dig

- Une résolution de nom classique


dig <NOM> pour obtenir l'IP associée à un nom
on teste la zone forward
```
[greg@marcel ~]$ dig dns1.server1.tp3

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> dns1.server1.tp3
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: REFUSED, id: 62444
;; flags: qr rd; QUERY: 1, ANSWER: 0, AUTHORITY: 0, ADDITIONAL: 1
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 77864073108920084008fe6d615c5cfd5b961a918ffd0e76 (good)
;; QUESTION SECTION:
;dns1.server1.tp3.              IN      A

;; Query time: 1 msec
;; SERVER: 10.3.1.2#53(10.3.1.2)
;; WHEN: Tue Oct 05 16:11:09 CEST 2021
;; MSG SIZE  rcvd: 73

[greg@marcel ~]$
```
prouvez que c'est bien votre serveur DNS qui répond pour chaque dig
```
On peut voir à la fin du dig juste avant, que le serveur utilisé est bien mon serveur DNS en 10.3.1.2

// ;; SERVER: 10.3.1.2#53(10.3.1.2) \\

[greg@marcel ~]$ dig dns1.server1.tp3

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> dns1.server1.tp3
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: REFUSED, id: 62444
;; flags: qr rd; QUERY: 1, ANSWER: 0, AUTHORITY: 0, ADDITIONAL: 1
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 77864073108920084008fe6d615c5cfd5b961a918ffd0e76 (good)
;; QUESTION SECTION:
;dns1.server1.tp3.              IN      A

;; Query time: 1 msec
;; SERVER: 10.3.1.2#53(10.3.1.2)
;; WHEN: Tue Oct 05 16:11:09 CEST 2021
;; MSG SIZE  rcvd: 73

[greg@marcel ~]$

<!-----------------------Exemple 2  --------------------->

[greg@dhcp ~]$ dig dns1.server1.tp3
[etc... ]
;; SERVER: 10.3.1.2#53(10.3.1.2)
;; WHEN: Tue Oct 05 16:13:24 CEST 2021
;; MSG SIZE  rcvd: 73

[greg@dhcp ~]$
```

##### 🌞 Configurez l'utilisation du serveur DNS sur TOUS vos noeuds

les serveurs, on le fait à la main

les clients, c'est fait via DHCP

```
[greg@marcel ~]$ dig dns1.server1.tp3

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> dns1.server1.tp3
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: REFUSED, id: 62444
;; flags: qr rd; QUERY: 1, ANSWER: 0, AUTHORITY: 0, ADDITIONAL: 1
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 77864073108920084008fe6d615c5cfd5b961a918ffd0e76 (good)
;; QUESTION SECTION:
;dns1.server1.tp3.              IN      A

;; Query time: 1 msec
;; SERVER: 10.3.1.2#53(10.3.1.2)
;; WHEN: Tue Oct 05 16:11:09 CEST 2021
;; MSG SIZE  rcvd: 73

[greg@marcel ~]$

<!-----------------------Exemple 2  --------------------->

[greg@dhcp ~]$ dig dns1.server1.tp3
[etc... ]
;; SERVER: 10.3.1.2#53(10.3.1.2)
;; WHEN: Tue Oct 05 16:13:24 CEST 2021
;; MSG SIZE  rcvd: 73

[greg@dhcp ~]$


<!-----------------------Exemple 3  --------------------->
[greg@routeur ~]$ dig dhcp.client1.tp3
[etc...]
;; SERVER: 10.3.1.2#53(10.3.1.2)
;; WHEN: Tue Oct 05 16:16:31 CEST 2021
;; MSG SIZE  rcvd: 73

[greg@routeur ~]$

<!-----------------------Exemple 4  --------------------->

[greg@dns1 ~]$ dig routeur.tp3

[etc...]
;; SERVER: 10.3.1.2#53(10.3.1.2)
;; WHEN: Tue Oct 05 16:18:18 CEST 2021
;; MSG SIZE  rcvd: 68

```

#### 4. Get deeper

##### 🌞 Affiner la configuration du DNS

faites en sorte que votre DNS soit désormais aussi un forwarder DNS
c'est à dire que s'il ne connaît pas un nom, il ira poser la question à quelqu'un d'autre


Hint : c'est la clause recursion dans le fichier /etc/named.conf. Et c'est déjà activé par défaut en fait.

##### 🌞 Test !

vérifier depuis marcel.client1.tp3 que vous pouvez résoudre des noms publics comme google.com en utilisant votre propre serveur DNS (commande dig)
pour que ça fonctionne, il faut que dns1.server1.tp3 soit lui-même capable de résoudre des noms, avec 1.1.1.1 par exemple

```
[greg@marcel ~]$ dig 1.1.1.1

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> 1.1.1.1
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: REFUSED, id: 19454
;; flags: qr rd; QUERY: 1, ANSWER: 0, AUTHORITY: 0, ADDITIONAL: 1
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 100b912af13372be7498c166615c5ff9f0ef22a1e49ea9b2 (good)
;; QUESTION SECTION:
;1.1.1.1.                       IN      A

;; Query time: 0 msec
;; SERVER: 10.3.1.2#53(10.3.1.2)
;; WHEN: Tue Oct 05 16:23:53 CEST 2021
;; MSG SIZE  rcvd: 64

<!-----------------------Exemple 2  --------------------->

[greg@marcel ~]$ dig amazon.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> amazon.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: REFUSED, id: 42051
;; flags: qr rd; QUERY: 1, ANSWER: 0, AUTHORITY: 0, ADDITIONAL: 1
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: e39bac980cbb280cee61b430615c600654d98e4be4e9039e (good)
;; QUESTION SECTION:
;amazon.com.                    IN      A

;; Query time: 0 msec
;; SERVER: 10.3.1.2#53(10.3.1.2)
;; WHEN: Tue Oct 05 16:24:06 CEST 2021
;; MSG SIZE  rcvd: 67

[greg@marcel ~]$
```

##### 🌞 Affiner la configuration du DHCP

faites en sorte que votre DHCP donne désormais l'adresse de votre serveur DNS aux clients
créer un nouveau client johnny.client1.tp3 qui récupère son IP, et toutes les nouvelles infos, en DHCP

```
La machine johnny recupere bien les infos du dhcp et du dns 

[greg@johnny ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:4f:97:53 brd ff:ff:ff:ff:ff:ff
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ed:00:9d brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.132/26 brd 10.3.1.191 scope global dynamic noprefixroute enp0s8
       valid_lft 891sec preferred_lft 891sec
    inet6 fe80::a00:27ff:feed:9d/64 scope link
       valid_lft forever preferred_lft forever

       
[greg@johnny ~]$ dig youtube.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> youtube.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: REFUSED, id: 7348
;; flags: qr rd; QUERY: 1, ANSWER: 0, AUTHORITY: 0, ADDITIONAL: 1
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 27e1a4b302d14c3cbbb7ceae615c61c777eb37d363cf29a5 (good)
;; QUESTION SECTION:
;youtube.com.                   IN      A

;; Query time: 1 msec
;; SERVER: 10.3.1.2#53(10.3.1.2)
;; WHEN: Tue Oct 05 16:45:49 CEST 2021
;; MSG SIZE  rcvd: 68

```
#### 5. Serveur Web

#### 🌞 Test test test et re-test

testez que votre serveur web est accessible depuis marcel.client1.tp3

utilisez la commande curl pour effectuer des requêtes HTTP
```
Grace au curl , on peut voir que la connexion marche bien entre marcel et le serveur web 

[greg@marcel ~]$ curl 10.3.1.194:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
```
#### 6. Le setup wola

##### 🌞 Setup d'une nouvelle machine, qui sera un serveur NFS

réseau server2

son nooooom : nfs1.server2.tp3 !
📝checklist📝
je crois que vous commencez à connaître la chanson... Google "nfs server rocky linux"

ce lien me semble être particulièrement simple et concis


vous partagerez un dossier créé à cet effet : /srv/nfs_share/
```
On peut voir que le partage de fchier est bon du côté nfs 

[greg@nsf1 ~]$ sudo mkdir /srv/nfs_share/
[greg@nsf1 ~]$ systemctl enable --now rpcbind nfs-server
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-unit-files ====
Authentication is required to manage system service or unit files.
Authenticating as: greg (greg)
Password:
==== AUTHENTICATION COMPLETE ====
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ====
Authentication is required to reload the systemd state.
Authenticating as: greg (greg)
Password:
==== AUTHENTICATION COMPLETE ====
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'rpcbind.service'.
Authenticating as: greg (greg)
Password:
==== AUTHENTICATION COMPLETE ====
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'nfs-server.service'.
Authenticating as: greg (greg)
Password:
==== AUTHENTICATION COMPLETE ====
[greg@nsf1 ~]$ firewall-cmd --add-service=nfs
Authorization failed.
    Make sure polkit agent is running or run the application as superuser.
[greg@nsf1 ~]$ sudo firewall-cmd --add-service=nfs
success
[greg@nsf1 ~]$ sudo firewall-cmd --add-service=nfs  --permanent
success
```
##### 🌞 Configuration du client NFS

effectuez de la configuration sur web1.server2.tp3 pour qu'il accède au partage NFS
le partage NFS devra être monté dans /srv/nfs/
sur le même site, y'a ça
```
Le montage sur le serveur web , c'est bien fait sur srv/nfs .

[greg@web1 ~]$ sudo mount -t nfs 10.3.1.195:/srv/nfs_share/ /srv/nfs
[greg@web1 ~]$ df -hT
Filesystem                Type      Size  Used Avail Use% Mounted on
devtmpfs                  devtmpfs  891M     0  891M   0% /dev
tmpfs                     tmpfs     909M     0  909M   0% /dev/shm
tmpfs                     tmpfs     909M  8.6M  901M   1% /run
tmpfs                     tmpfs     909M     0  909M   0% /sys/fs/cgroup
/dev/mapper/rl-root       xfs       6.2G  2.1G  4.1G  34% /
/dev/sda1                 xfs      1014M  301M  714M  30% /boot
tmpfs                     tmpfs     182M     0  182M   0% /run/user/1000
10.3.1.195:/srv/nfs_share nfs4      6.2G  2.1G  4.1G  34% /srv/nfs
[greg@web1 ~]$

```

##### 🌞 TEEEEST

tester que vous pouvez lire et écrire dans le dossier /srv/nfs depuis web1.server2.tp3

```
Point de vu du web 1 : 

[greg@web1 ~]$ sudo vi /srv/nfs/test.nfs
[sudo] password for greg:
[greg@web1 ~]$ [greg@web1 ~]$ sudo vi /srv/nfs/test_nfs
[greg@web1 ~]$ sudo ls /srv/nfs/test_nfs
/srv/nfs/test_nfs
[greg@web1 ~]$
```

vous devriez voir les modifications du côté de  nfs1.server2.tp3 dans le dossier /srv/nfs_share/
```
Point de vu de nfs1 : 
[greg@nsf1 ~]$ [greg@nsf1 ~]$ sudo ls /srv/nfs_share/
test_nfs
[greg@nsf1 ~]$
```

##### 🌞 Déterminer, pour chacun de ces protocoles, s'ils sont encapsulés dans du TCP ou de l'UDP :

SSH = TCP

HTTP = TCP

DNS = UDP

NFS= TCP

##### 📁 Captures réseau tp3_ssh.pcap, tp3_http.pcap, tp3_dns.pcap et tp3_nfs.pcap

Prenez le temps de réfléchir à pourquoi on a utilisé TCP ou UDP pour transmettre tel ou tel protocole. Réfléchissez à quoi servent chacun de ces protocoles, et de ce qu'on a besoin qu'ils réalisent.

##### 🌞 Capturez et mettez en évidence un 3-way handshake
##### 📁 Capture réseau tp3_3way.pcap

#### 8. El final
##### 🌞 Bah j'veux un schéma.


🌞 Et j'veux des fichiers aussi, tous les fichiers de conf du DNS

📁 Fichiers de zone

📁 Fichier de conf principal DNS named.conf

#####  Tableau des réseaux + adressage
```
| Nom du réseau | Adresse du réseau | Masque            | Nombre de clients possibles | Adresse passerelle | Adresse broadcast |
| ------------- | ----------------- | ----------------- | --------------------------- | ------------------ | ----------------- |
| `server1`     | `10.3.1.0`        | `255.255.255.128` | `126`                       | `10.3.1.126`       | `10.3.1.127`      |
| `client1`     | `10.3.1.128`      | `255.255.255.192` | `62`                        | `10.3.1.190`       | `10.3.1.191`      |
| `server2`     | `10.3.1.192`      | `255.255.255.240` | `14`                        | `10.3.1.206`       | `10.3.1.207`      |



| Nom machine          | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
| -------------------- | -------------------- | -------------------- | -------------------- | --------------------- |
| `router.tp3`         | `10.3.1.190/26`      | `10.3.1.126/25`      | `10.3.1.206/28`      | Carte NAT             |
| `dhcp.client1.tp3`   | `10.3.1.130/26`      | x                    | x                    | x                     |
| `dns1.server1.tp3`   | x                    | `10.3.1.2/25`        | x                    | x                     |
| `marcel.client1.tp3` | `10.3.1.131/26`      |                      | x                    | x                     |
| `johnny.client1.tp3` | `10.3.1.132/26`      |                      | x                    | x                     |
| `web1.server2.tp3`   | x                    |                      | `10.3.1.194/28`      | x                     |
| `nfs1.server2.tp3`   | x                    |                      | `10.3.1.195/28`      | x                     |
```

##### Schema du réseau

