#!/bin/bash
# Script de backup 
# Greg - 23/10/2021

usage() {
        echo "usage: ./tp2_backup.sh destination dir_to_backup"
        echo "  destination       Path to the directory that stores backups"
        echo "  dir_to_backup     Path to a directory to backup"
        exit 1
}

if [[ -z $1 || -z $2 ]] ; then
        >&2 echo "You need to specify a destination and a directory to backup"
        >&2 echo
        usage
fi

clean_backup() {
        # Keep 5 backups by default
        if [[ -z $qty_to_keep ]] ; then qty_to_keep=5 ; fi

        qty_to_keep_tail=$((qty_to_keep+1))
        ls -tp "${destination}" | grep -v '/$' | tail -n +${qty_to_keep_tail} | xargs -I {} rm -- ${destination}/{}
        status=$?
        if [[ $status -eq 0 ]] ; then
                echo -e "[OK] Directory ${destination} cleaned to keep only the ${qty_to_keep} most recent backups."
        else
                >&2 echo -e "[ERROR] Failed to clean ${destination}. Old backups may remain."
        exit 1
        fi
}

destination=$1
dir_to_backup=$2
backup_name=$(date +tp2_backup_%y%m%d_%H%M%S.tar.gz)
backup_fullpath="$(pwd)/${backup_name}"

tar cvzf "${backup_fullpath}" "${dir_to_backup}"
if [[ $? -eq 0 ]] ; then
        echo "Archive: ${backup_name} successfully created in ${destination}."
else
        >&2 echo "Failed trying to archive ${dir_to_backup}"
        exit 1
fi

rsync -av --remove-source-files "${backup_fullpath}" "${destination}"

if [[ $? -eq 0 ]] ; then
        echo "Archive: ${backup_name} successfully synchronized to ${dir_to_backup}."
else
        >&2 echo "Failed trying to sync ${dir_to_backup}"
        exit 1
fi

clean_backup 5