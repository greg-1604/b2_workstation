# B2_TP2-Part2_Linux

## I. Monitoring

### 2. Setup

#### 🌞 Setup Netdata

```
[greg@web ~]$ sudo su -
[sudo] password for greg:
[root@web ~]# bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
 --- Downloading static netdata binary: https://storage.googleapis.com/netdata-nightlies/netdata-latest.gz.run ---
[/tmp/netdata-kickstart-q2Djsn3Mlx]# curl -q -sSL --connect-timeout 10 --retry 3 --output /tmp/netdata-kickstart-q2Djsn3Mlx/sha256sum.txt https://storage.googleapis.com/netdata-nightlies/sha256sums.txt
 OK

[/tmp/netdata-kickstart-q2Djsn3Mlx]# curl -q -sSL --connect-timeout 10 --retry 3 --output /tmp/netdata-kickstart-q2Djsn3Mlx/netdata-latest.gz.run https://storage.googleapis.com/netdata-nightlies/netdata-latest.gz.run
 OK

 --- Installing netdata ---
[/tmp/netdata-kickstart-q2Djsn3Mlx]# sh /tmp/netdata-kickstart-q2Djsn3Mlx/netdata-latest.gz.run -- --auto-update

  ^
  |.-.   .-.   .-.   .-.   .  netdata
  |   '-'   '-'   '-'   '-'   real-time performance monitoring, done right!
  +----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+--->

  (C) Copyright 2017, Costa Tsaousis
  All rights reserved
  Released under GPL v3+

  You are about to install netdata to this system.
  netdata will be installed at:

                    /opt/netdata

  The following changes will be made to your system:

  # USERS / GROUPS
  User 'netdata' and group 'netdata' will be added, if not present.

  # LOGROTATE
  This file will be installed if logrotate is present.

   - /etc/logrotate.d/netdata

  # SYSTEM INIT
  This file will be installed if this system runs with systemd:

   - /lib/systemd/system/netdata.service

   or, for older CentOS, Debian/Ubuntu or OpenRC Gentoo:

   - /etc/init.d/netdata         will be created


  This package can also update a netdata installation that has been
  created with another version of it.

  Your netdata configuration will be retained.
  After installation, netdata will be (re-)started.

  netdata re-distributes a lot of open source software components.
  Check its full license at:
  https://github.com/netdata/netdata/blob/master/LICENSE.md
Please type y to accept, n otherwise: y
Creating directory /opt/netdata
Verifying archive integrity...  100%   All good.
Uncompressing netdata, the real-time performance and health monitoring system 100%
 --- Deleting stock configuration files from user configuration directory ---
 --- Attempt to create user/group netdata/netadata ---
Adding netdata user group ...
[/opt/netdata]# groupadd -r netdata
 OK

Adding netdata user account with home /opt/netdata ...
[/opt/netdata]# useradd -r -g netdata -c netdata -s /usr/sbin/nologin --no-create-home -d /opt/netdata netdata
 OK

 --- Add user netdata to required user groups ---
Group 'docker' does not exist.
 FAILED  Failed to add netdata user to secondary groups

Group 'nginx' does not exist.
 FAILED  Failed to add netdata user to secondary groups

Group 'varnish' does not exist.
 FAILED  Failed to add netdata user to secondary groups

Group 'haproxy' does not exist.
 FAILED  Failed to add netdata user to secondary groups

Adding netdata user to the adm group ...
[/opt/netdata]# usermod -a -G adm netdata
 OK

Group 'nsd' does not exist.
 FAILED  Failed to add netdata user to secondary groups

Group 'proxy' does not exist.
 FAILED  Failed to add netdata user to secondary groups

Group 'squid' does not exist.
 FAILED  Failed to add netdata user to secondary groups

Group 'ceph' does not exist.
 FAILED  Failed to add netdata user to secondary groups

Adding netdata user to the nobody group ...
[/opt/netdata]# usermod -a -G nobody netdata
 OK

Group 'I2C' does not exist.
 FAILED  Failed to add netdata user to secondary groups

 --- Install logrotate configuration for netdata ---
[/opt/netdata]# cp system/netdata.logrotate /etc/logrotate.d/netdata
 OK

[/opt/netdata]# chmod 644 /etc/logrotate.d/netdata
 OK

 --- Telemetry configuration ---
You can opt out from anonymous statistics via the --disable-telemetry option, or by creating an empty file /opt/netdata/etc/netdata/.opt-out-from-anonymous-statistics

 --- Install netdata at system init ---
Installing systemd service...
[/opt/netdata]# cp system/netdata.service /lib/systemd/system/netdata.service
 OK

[/opt/netdata]# systemctl daemon-reload
 OK

[/opt/netdata]# systemctl enable netdata
Created symlink /etc/systemd/system/multi-user.target.wants/netdata.service -> /usr/lib/systemd/system/netdata.service.
 OK

 --- Install (but not enable) netdata updater tool ---
Failed to disable unit: Unit file netdata-updater.timer does not exist.
cat: /system/netdata-updater.timer: No such file or directory
cat: /system/netdata-updater.service: No such file or directory
Update script is located at /opt/netdata/usr/libexec/netdata/netdata-updater.sh

 --- Check if we must enable/disable the netdata updater tool ---
Auto-updating has been enabled through cron, updater script linked to /etc/cron.daily/netdata-updater

If the update process fails and you have email notifications set up correctly for cron on this system, you should receive an email notification of the failure.
Successful updates will not send an email.

 --- creating quick links ---
[/opt/netdata]# ln -s bin sbin
 OK

[/opt/netdata/usr]# ln -s ../bin bin
 OK

[/opt/netdata/usr]# ln -s ../bin sbin
 OK

[/opt/netdata/usr]# ln -s . local
 OK

[/opt/netdata]# ln -s etc/netdata netdata-configs
 OK

[/opt/netdata]# ln -s usr/share/netdata/web netdata-web-files
 OK

[/opt/netdata]# ln -s usr/libexec/netdata netdata-plugins
 OK

[/opt/netdata]# ln -s var/lib/netdata netdata-dbs
 OK

[/opt/netdata]# ln -s var/cache/netdata netdata-metrics
 OK

[/opt/netdata]# ln -s var/log/netdata netdata-logs
 OK

[/opt/netdata/etc/netdata]# rm orig
 OK

[/opt/netdata/etc/netdata]# ln -s ../../usr/lib/netdata/conf.d orig
 OK

 --- fix permissions ---
[/opt/netdata]# chmod g+rx\,o+rx /opt
 OK

[/opt/netdata]# chown -R netdata:netdata /opt/netdata
 OK

 --- fix plugin permissions ---
[/opt/netdata]# chown root:netdata usr/libexec/netdata/plugins.d/apps.plugin
 OK

[/opt/netdata]# chmod 4750 usr/libexec/netdata/plugins.d/apps.plugin
 OK

[/opt/netdata]# chown root:netdata usr/libexec/netdata/plugins.d/ioping
 OK

[/opt/netdata]# chmod 4750 usr/libexec/netdata/plugins.d/ioping
 OK

[/opt/netdata]# chown root:netdata usr/libexec/netdata/plugins.d/cgroup-network
 OK

[/opt/netdata]# chmod 4750 usr/libexec/netdata/plugins.d/cgroup-network
 OK

[/opt/netdata]# chown root:netdata bin/fping
 OK

[/opt/netdata]# chmod 4750 bin/fping
 OK

Configure TLS certificate paths
Using /etc/pki/tls for TLS configuration and certificates
Save install options
 --- starting netdata ---
 --- Restarting netdata instance ---

Stopping all netdata threads
[/opt/netdata]# stop_all_netdata
 OK

Starting netdata using command 'systemctl start netdata'
[/opt/netdata]# systemctl start netdata
 OK

Downloading default configuration from netdata...
[/opt/netdata]# curl -sSL --connect-timeout 10 --retry 3 http://localhost:19999/netdata.conf
 OK

[/opt/netdata]# mv /opt/netdata/etc/netdata/netdata.conf.new /opt/netdata/etc/netdata/netdata.conf
 OK

 OK  New configuration saved for you to edit at /opt/netdata/etc/netdata/netdata.conf


  ^
  |.-.   .-.   .-.   .-.   .-.   .  netdata              .-.   .-.   .-.   .-
  |   '-'   '-'   '-'   '-'   '-'   is installed now!  -'   '-'   '-'   '-'
  +----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+--->

[/opt/netdata]# chmod 0644 /opt/netdata/etc/netdata/netdata.conf
 OK

 OK

[/tmp/netdata-kickstart-q2Djsn3Mlx]# rm /tmp/netdata-kickstart-q2Djsn3Mlx/netdata-latest.gz.run
 OK

[/tmp/netdata-kickstart-q2Djsn3Mlx]# rm -rf /tmp/netdata-kickstart-q2Djsn3Mlx
 OK

[root@web ~]# exit
logout
```

#### 🌞 Manipulation du service Netdata

- Déterminer s'il est actif, et s'il est paramétré pour démarrer au boot de la machine

```
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: >
   Active: active (running) since Tue 2021-10-12 14:58:32 CEST; 24min ago
  Process: 2106 ExecStartPre=/bin/chown -R netdata:netdata /opt/netdata/var/run/netd>
  Process: 2104 ExecStartPre=/bin/mkdir -p /opt/netdata/var/run/netdata (code=exited>
  Process: 2102 ExecStartPre=/bin/chown -R netdata:netdata /opt/netdata/var/cache/ne>
  Process: 2101 ExecStartPre=/bin/mkdir -p /opt/netdata/var/cache/netdata (code=exit>
 Main PID: 2108 (netdata)
    Tasks: 35 (limit: 4946)
   Memory: 55.1M
   CGroup: /system.slice/netdata.service
           ├─2108 /opt/netdata/bin/srv/netdata -P /opt/netdata/var/run/netdata/netda>
           ├─2117 /opt/netdata/bin/srv/netdata --special-spawn-server
           ├─2277 /opt/netdata/usr/libexec/netdata/plugins.d/apps.plugin 1
           └─2284 /opt/netdata/usr/libexec/netdata/plugins.d/go.d.plugin 1
```

- Déterminer à l'aide d'une commande ss sur quel(s) port(s) Netdata écoute

```
[greg@web ~]$ sudo ss -alptn | grep netdata
LISTEN 0      128        127.0.0.1:8125       0.0.0.0:*    users:(("netdata",pid=2108,fd=35))                                                                             
LISTEN 0      128          0.0.0.0:19999      0.0.0.0:*    users:(("netdata",pid=2108,fd=5))                                                                              
LISTEN 0      128            [::1]:8125          [::]:*    users:(("netdata",pid=2108,fd=34))                                                                             
LISTEN 0      128             [::]:19999         [::]:*    users:(("netdata",pid=2108,fd=6))
```

- Autoriser ce port dans le firewall

```
[greg@web ~]$ sudo firewall-cmd --add-port 19999/tcp
success
[greg@web ~]$ sudo firewall-cmd --add-port 19999/tcp --permanent
success
```

#### 🌞 Setup Alerting

```
bash-4.4$ /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test

[...]

2021-10-12 15:59:04: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is CLEAR to 'alarms'
# OK
```

#### 🌞 Config alerting

- Créez une nouvelle alerte pour recevoir une alerte à 50% de remplissage de la RAM

```
    alarm: ram_in_use
       on: system.ram
    class: Utilization
     type: System
component: Memory
       os: linux
    hosts: *
     calc: ($used - $used_ram_to_ignore) * 100 / ($used + $cached + $free + $buffers)
    units: %
    every: 10s
     warn: $this > (($status >= $WARNING)  ? (50) : (50))
     crit: $this > (($status == $CRITICAL) ? (90) : (98))
    delay: down 15m multiplier 1.5 max 1h
     info: Percentage of used RAM. \
           High RAM utilization. \
           It may affect the performance of applications. \
           If there is no swap space available, OOM Killer can start killing processes. \
           You might want to check per-process memory usage to find the top consumers.
       to: sysadmin
```

- Testez que votre alerte fonctionne

```
web.tp2.linux is critical, system.cpu (cpu), 10min cpu iowait = 53.3%
```

## II. Backup

### 2. Partage NFS

#### 🌞 Setup environnement

- Créer un dossier /srv/backups/

```
[greg@backup ~]$ ls -al /srv
total 0
[...]
drwxr-xr-x.  3 root root  27 Oct 12 18:07 backups
drwxr-xr-x.  2 root root  22 Oct  6 17:34 nfs_share
```

- Commencez donc par créer le dossier /srv/backups/web.tp2.linux/

```
[greg@backup ~]$ ls -al /srv/backups/
total 0
drwxr-xr-x. 3 root root 27 Oct 12 18:07 .
drwxr-xr-x. 4 root root 38 Oct 12 18:07 ..
drwxr-xr-x. 2 root root  6 Oct 12 18:07 web.tp2.linux
```

#### 🌞 Setup partage NFS

```
  374  sudo dnf -y install nfs-utils
  376  sudo vim /etc/idmapd.conf
  380  sudo vim /etc/exports
  381  sudo systemctl enable --now rpcbind nfs-server
  382  sudo firewall-cmd --add-service=nfs
```

#### 🌞 Setup points de montage sur web.tp2.linux

```
[greg@web ~]$ df -hT
Filesystem                             Type      Size  Used Avail Use% Mounted on
devtmpfs                               devtmpfs  387M     0  387M   0% /dev
tmpfs                                  tmpfs     405M  332K  405M   1% /dev/shm
tmpfs                                  tmpfs     405M  5.6M  400M   2% /run
tmpfs                                  tmpfs     405M     0  405M   0% /sys/fs/cgroup
/dev/mapper/rl-root                    xfs       6.2G  3.7G  2.6G  59% /
/dev/sda1                              xfs      1014M  301M  714M  30% /boot
tmpfs                                  tmpfs      81M     0   81M   0% /run/user/1000
10.102.1.13:/srv/backups/web.tp2.linux nfs4      6.2G  2.2G  4.1G  35% /srv/backup
```

```
[greg@web ~]$ sudo vim /etc/fstab (Ecrire dans le fichier la ligne suivante :)
10.102.1.13:/srv/backups/web.tp2.linux /srv/backup/ nfs defaults        0 0
```

### 3. Backup de fichiers

#### 🌞 Rédiger le script de backup /srv/tp2_backup.sh

Voir dossier join

#### 🌞 Tester le bon fonctionnement

- Exécuter le script sur le dossier de votre choix

```
[greg@backup srv]$ sudo mkdir dir1 dir2
[greg@backup srv]$ sudo touch dir2/hey

[greg@backup srv]$ sudo ./tp2_backup.sh dir1/ dir2/
dir1/
Archive: tp2_backup_211022_232948.tar.gz successfully created in dir1/.
sending incremental file list
tp2_backup_211022_232948.tar.gz

sent 225 bytes  received 43 bytes  536.00 bytes/sec
total size is 109  speedup is 0.41
Archive: tp2_backup_211022_232948.tar.gz successfully synchronized to dir1/.
```

# prouvez que la backup s'est bien exécutée

```
[greg@backup srv]$ echo $?
0
[greg@backup srv]$ ls dir1
tp2_backup_251022_101363.tar.gz
```

- Tester de restaurer les données

```
[greg@backup dir1]$ sudo tar -xf tp2_backup_211022_232948.tar.gz
[greg@backup dir1]$ ls
dir2  tp2_backup_211022_232948.tar.gz
[greg@backup dir1]$ ls dir2/
hey
```

### 4. Unité de service

#### 🌞 Créer une unité de service pour notre backup

```
[greg@web ~]$ sudo vim /etc/systemd/system/tp2_backup.service
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/srv/tp2_backup.sh /srv/dir1  /srv/dir2
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```

#### 🌞 Tester le bon fonctionnement

- N'oubliez pas d'exécuter sudo systemctl daemon-reload à chaque ajout/modification d'un service

```
[greg@web ~]$ sudo systemctl daemon-reload
```

- Essayez d'effectuer une sauvegarde avec sudo systemctl start backup

```
[greg@web srv]$ sudo systemctl start tp2_backup
```

- Prouvez que la backup s'est bien exécutée

```
[greg@web srv]$ ls dir1/
tp2_backup_211023_171825.tar.gz
```

#### 🌞 Créer le timer associé à notre tp2_backup.service

```
[greg@web ~]$ sudo vim /etc/systemd/system/tp2_backup.timer
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp2_backup.service

[Timer]
Unit=tp2_backup.service
OnCalendar=*-*-* *:*:00

[Install]
WantedBy=timers.target
```

#### 🌞 Activez le timer

- Démarrer le timer : sudo systemctl start tp2_backup.timer

```
[greg@web srv]$ sudo systemctl start tp2_backup.timer
```

- Activer le au démarrage avec une autre commande systemctl

```
[greg@web srv]$ sudo systemctl enable tp2_backup.timer
Created symlink /etc/systemd/system/timers.target.wants/tp2_backup.timer → /etc/systemd/system/tp2_backup.timer.
```

- Le timer est actif actuellement

```
[greg@web srv]$ sudo systemctl is-active tp2_backup.timer
active
```
- Qu'il est paramétré pour être actif dès que le système boot

```
[greg@web srv]$ sudo systemctl is-enabled tp2_backup.timer
enabled
```

#### 🌞 Tests !

- Il y a bien une archive par minute et la backup s'execute correctement

```
[greg@web srv]$ ls dir1
tp2_backup_211023_225534.tar.gz  tp2_backup_211023_225734.tar.gz  tp2_backup_211023_225945.tar.gz
tp2_backup_211023_225609.tar.gz  tp2_backup_211023_225834.tar.gz
```

#### 🌞 Faites en sorte que...

- Votre backup s'exécute sur la machine web.tp2.linux

```
[greg@web ~]$ hostname
web.tp2.linux
```

- Le dossier sauvegardé est celui qui contient le site NextCloud (quelque part dans /var/)
- La destination est le dossier NFS monté depuis le serveur backup.tp2.linux

```
[greg@web ~]$ sudo cat /etc/systemd/system/tp2_backup.service
[...]
ExecStart=/srv/tp2_backup.sh /srv/backup /var/www/sub-domains/com.web.nextcloud
[...]
```

- La sauvegarde s'exécute tous les jours à 03h15 du matin

```
[greg@web ~]$ sudo cat /etc/systemd/system/tp2_backup.timer
[...]
OnCalendar=*-*-* 3:15:00
[...]
```

- Prouvez avec la commande sudo systemctl list-timers que votre service va bien s'exécuter la prochaine fois qu'il sera 03h15

```
[greg@web srv]$ sudo systemctl list-timers
NEXT                          LEFT          LAST                          PASSED      UNIT                         ACTIVATES
Sat 2021-10-25 03:15:00 CEST  3h left  n/a                           n/a          tp2_backup.timer             tp2_backup.service
```

### 5. Backup de base de données

#### 🌞 Création d'un script /srv/tp2_backup_db.sh

- Test

```
[greg@db ~]$ sudo /srv/tp2_backup_db.sh ./dir_test/ nextcloud
mysqldump: Got errno 32 on write
[greg@db ~]$ ls dir_test/
db_backup_211025_211628.sql.tar.gz
```

#### 🌞 Restauration

- Tester la restauration de données

```
[greg@db dir_test]$ mysql -h localhost -u root --password=greg nextcloud < db_backup_211025_211916.sql.tar.gz
```

#### 🌞 Unité de service

- Preuve du bon fonctionnement du service

```
[greg@db ~]$ sudo systemctl daemon-reload
[greg@db ~]$ sudo systemctl start tp2_backup_db
[greg@db ~]$ sudo systemctl enable tp2_backup_db
[greg@db ~]$ sudo systemctl is-active tp2_backup_db
active
[greg@db ~]$ sudo systemctl is-enabled tp2_backup_db
enabled
[greg@db dir_test]$ ls
db_backup_211025_212346.sql.tar.gz
```

- Preuve du bon fonctionnement du timer

```
[greg@db dir_test]$ sudo systemctl list-timers
NEXT                          LEFT          LAST                          PASSED    UNIT                         ACTIVATES
[...]
Sun 2021-10-25 21:26:12 CEST  18h 11min left n/a                           n/a       tp2_backup_db.timer          tp2_backup_db.service
[...]
```

## III. Reverse Proxy

### 2. Setup simple

#### 🌞 Installer NGINX

- Vous devrez d'abord installer le paquet epel-release avant d'installer nginx

```
[greg@front ~]$ sudo dnf install epel-release nginx
```

#### 🌞 Tester !

- Lancer le service nginx

```
[greg@front ~]$ sudo systemctl start nginx
[greg@front ~]$ sudo systemctl is-active nginx
active
```

- Le paramétrer pour qu'il démarre seul quand le système boot

```
[greg@front ~]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
[greg@front ~]$ sudo systemctl is-enabled nginx
enabled
```

- Repérer le port qu'utilise NGINX par défaut, pour l'ouvrir dans le firewall

```
[greg@front ~]$ sudo ss -lpnt
State       Recv-Q      Send-Q            Local Address:Port             Peer Address:Port      Process
[...]
LISTEN      0           128                     0.0.0.0:80                    0.0.0.0:*          users:(("nginx",pid=4201,fd=8),("nginx",pid=4200,fd=8))
[...]
LISTEN      0           128                        [::]:80                       [::]:*          users:(("nginx",pid=4201,fd=9),("nginx",pid=4200,fd=9))
```

- Vérifier que vous pouvez joindre NGINX avec une commande curl depuis votre PC

```
[greg@front ~]$ sudo firewall-cmd --add-port 80/tcp
success

PS C:\Users\nicol> curl 10.102.1.14:80


StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
[...]
```

#### 🌞 Explorer la conf par défaut de NGINX

```
[greg@front ~]$ sudo cat /etc/nginx/nginx.conf
[...]
```

- Repérez l'utilisateur qu'utilise NGINX par défaut

```
user nginx;
[...]
```

- Mettez en évidence ces lignes d'inclusion dans le fichier de conf principal

```
include /usr/share/nginx/modules/*.conf;

[...]
http {
  
    [...]
    include /etc/nginx/conf.d/*.conf;

    server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /usr/share/nginx/html;

        include /etc/nginx/default.d/*.conf;

        location / {
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }
[...]
}
```

#### 🌞 Modifier la conf de NGINX

- Pour que ça fonctionne, le fichier /etc/hosts de la machine DOIT être rempli correctement, conformément à la 📝checklist📝

```
[greg@front ~]$ sudo cat /etc/hosts
[...]
10.102.1.11 web.tp2.linux
10.102.1.12 db.tp2.linux
10.102.1.13 backup.tp2.linux
```

- Supprimer le bloc server {} par défaut, pour ne plus présenter la page d'accueil NGINX

```
[greg@front ~]$ sudo cat /etc/nginx/nginx.conf
user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 1024;
}

http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 2048;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    include /etc/nginx/conf.d/*.conf;
}
```

- Créer un fichier /etc/nginx/conf.d/web.tp2.linux.conf avec le contenu suivant :

```
[greg@front ~]$ sudo cat /etc/nginx/conf.d/web.tp2.linux.conf
server {
    listen 80;
    server_name web.tp2.linux; 
    location / {

        proxy_pass http://web.tp2.linux;
    }
}
```

## IV. Firewalling

### 2. Mise en place

#### 🌞 Restreindre l'accès à la base de données db.tp2.linux

```
[greg@db dir_test]$ sudo firewall-cmd --set-default-zone=drop
success
```

- Seul le serveur Web doit pouvoir joindre la base de données sur le port 3306/tcp

```
[greg@db dir_test]$ sudo firewall-cmd --new-zone=web --permanent
success
[greg@db dir_test]$ sudo firewall-cmd --zone=web --add-source=10.102.1.11/32 --permanent
success
[greg@db dir_test]$ sudo firewall-cmd --zone=web --add-port=3306/tcp --permanent
success
```

- Vous devez aussi autoriser votre accès SSH

```
[greg@db dir_test]$ sudo firewall-cmd --new-zone=ssh --permanent
success
[greg@db dir_test]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
success
[greg@db dir_test]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent
success
```

#### 🌞 Montrez le résultat de votre conf avec une ou plusieurs commandes firewall-cmd

```
[greg@db dir_test]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
ssh
  sources: 10.102.1.1/32
web
  sources: 10.102.1.11/32

[greg@db dir_test]$ sudo firewall-cmd --get-default-zone
drop

[greg@db dir_test]$ sudo firewall-cmd --list-all --zone=web
web (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.11/32
  services:
  ports: 3306/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[greg@db dir_test]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[greg@db dir_test]$ sudo firewall-cmd --list-all --zone=drop
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

#### 🌞 Restreindre l'accès au serveur Web web.tp2.linux

```
[greg@web srv]$ sudo firewall-cmd --set-default-zone=drop
```

- Seul le reverse proxy front.tp2.linux doit accéder au serveur web sur le port 80

```
[greg@web srv]$ sudo firewall-cmd --new-zone=proxy --permanent
[greg@web srv]$ sudo firewall-cmd --zone=proxy --add-source=10.102.1.14/32 --permanent
[greg@web srv]$ sudo firewall-cmd --zone=proxy --add-port=80/tcp --permanent
```

- N'oubliez pas votre accès SSH

```
[greg@web srv]$ sudo firewall-cmd --new-zone=ssh --permanent
[greg@web srv]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
[greg@web srv]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent
```

🌞 Montrez le résultat de votre conf avec une ou plusieurs commandes firewall-cmd

```
[greg@web srv]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
proxy
  sources: 10.102.1.14/32
ssh
  sources: 10.102.1.1/32

[greg@web srv]$ sudo firewall-cmd --get-default-zone
drop

[greg@web srv]$ sudo firewall-cmd --list-all --zone=proxy
proxy (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.14/32
  services:
  ports: 80/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[greg@web srv]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[greg@web srv]$ sudo firewall-cmd --list-all --zone=drop
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

#### 🌞 Restreindre l'accès au serveur de backup backup.tp2.linux

```
[greg@backup backup]$ sudo firewall-cmd --set-default-zone=drop
```

- Seules les machines qui effectuent des backups doivent être autorisées à contacter le serveur de backup via NFS

```
[greg@backup backup]$ sudo firewall-cmd --new-zone=nfs --permanent
[greg@backup backup]$ sudo firewall-cmd --zone=nfs --add-source=10.102.1.11/32 --permanent
[greg@backup backup]$ sudo firewall-cmd --zone=nfs --add-source=10.102.1.12/32 --permanent
```

- N'oubliez pas votre accès SSH

```
[greg@backup backup]$ sudo firewall-cmd --new-zone=ssh --permanent
[greg@backup backup]$ sudo firewall-cmd --zone=nfs --add-source=10.102.1.1/32 --permanent
[greg@backup backup]$ sudo firewall-cmd --zone=nfs --add-port=22/tcp --permanent
```

#### 🌞 Montrez le résultat de votre conf avec une ou plusieurs commandes firewall-cmd

```
[greg@backup backup]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
nfs
  sources: 10.102.1.11/32 10.102.1.12/32
ssh
  sources: 10.102.1.1/32

[greg@backup backup]$ sudo firewall-cmd --get-default-zone
drop

[greg@backup backup]$ sudo firewall-cmd --list-all --zone=nfs
nfs (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.11/32 10.102.1.12/32
  services:
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[greg@backup backup]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[greg@backup backup]$ sudo firewall-cmd --list-all --zone=drop
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

#### 🌞 Restreindre l'accès au reverse proxy front.tp2.linux

```
[greg@front ~]$ sudo firewall-cmd --set-default-zone drop
```

- Seules les machines du réseau 10.102.1.0/24 doivent pouvoir joindre le proxy

```
[greg@front ~]$ sudo firewall-cmd --new-zone=proxy --permanent
[greg@front ~]$ sudo firewall-cmd --zone=proxy --add-source=10.102.1.0/24 --permanent
```

- N'oubliez pas votre accès SSH

```
[greg@front ~]$ sudo firewall-cmd --new-zone=ssh --permanent
[greg@front ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
[greg@front ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent
```

#### 🌞 Montrez le résultat de votre conf avec une ou plusieurs commandes firewall-cmd

```
[greg@front ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
proxy
  sources: 10.102.1.0/24
ssh
  sources: 10.102.1.1/32

[greg@front ~]$ sudo firewall-cmd --get-default-zone
drop

[greg@front ~]$ sudo firewall-cmd --list-all --zone=proxy
proxy (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.0/24
  services:
  ports: 80/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[greg@front ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[greg@front ~]$ sudo firewall-cmd --list-all --zone=drop
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

#### 🌞 Rendez-moi le tableau suivant, correctement rempli : 

| Machine            | IP            | Service                 | Port ouvert         | IPs autorisées                                    |
|--------------------|---------------|-------------------------|---------------------|---------------------------------------------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | `80/tcp` `22/tcp`   | `10.102.1.14/32` `10.102.1.1/32`                  |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | `3306/tcp` `22/tcp` | `10.102.1.11/32` `10.102.1.1/32`                  |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | `22/tcp`            | `10.102.1.11/32` `10.102.1.12/32` `10.102.1.1/32` |
| `front.tp2.linux`  | `10.102.1.14` | Reverse Proxy           | `80/tcp` `22/tcp`   | `10.102.1.0/24` `10.102.1.1/32`                   |
