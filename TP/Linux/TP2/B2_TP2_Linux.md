# B2_TP2_Linux

## I. Un premier serveur web

### 1. Installation

#### 🌞 Installer le serveur Apache

- Paquet httpd

Pour installer apache: [greg@web ~]$ sudo dnf install httpd

- Le fichier de conf principal est /etc/httpd/conf/httpd.conf
- Je vous conseille vivement de virer tous les commentaire du fichier, à défaut de les lire, vous y verrez plus clair
- Avec vim vous pouvez tout virer avec :g/^ *#.*/d

```
[greg@web ~]$ cat /etc/httpd/conf/httpd.conf

ServerRoot "/etc/httpd"

Listen 80

[...]
```

#### 🌞 Démarrer le service Apache

- Démarrez le

```
[greg@web ~]$ systemctl start httpd
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'httpd.service'.
Authenticating as: greg
Password:
==== AUTHENTICATION COMPLETE ====
```

- Faites en sorte qu'Apache démarre automatique au démarrage de la machine

```
[greg@web ~]$ systemctl enable httpd
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-unit-files ====
Authentication is required to manage system service or unit files.
Authenticating as: greg
Password:
==== AUTHENTICATION COMPLETE ====
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ====
Authentication is required to reload the systemd state.
Authenticating as: greg
Password:
==== AUTHENTICATION COMPLETE ====
```

- Ouvrez le port firewall nécessaire

```
[greg@web ~]$ sudo firewall-cmd --add-port=80/tcp
success
[greg@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
```

- Utiliser une commande ss pour savoir sur quel port tourne actuellement Apache

```
[greg@web ~]$ sudo ss -tlpn
State   Recv-Q  Send-Q   Local Address:Port    Peer Address:Port  Process
[...]
LISTEN  0       128                  *:80                 *:*      users:(("httpd",pid=1467,fd=4),("httpd",pid=1466,fd=4),("httpd",pid=1465,fd=4),("httpd",pid=1463,fd=4))
[...]
```

🌞 TEST

- Vérifier que le service est démarré

```
[greg@web ~]$ systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-10-06 15:01:59 CEST; 1h 5min ago
     Docs: man:httpd.service(8)
 Main PID: 1463 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 4946)
   Memory: 28.9M
   CGroup: /system.slice/httpd.service
           ├─1463 /usr/sbin/httpd -DFOREGROUND
           ├─1464 /usr/sbin/httpd -DFOREGROUND
           ├─1465 /usr/sbin/httpd -DFOREGROUND
           ├─1466 /usr/sbin/httpd -DFOREGROUND
           └─1467 /usr/sbin/httpd -DFOREGROUND

Oct 06 15:01:58 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Oct 06 15:01:59 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Oct 06 15:01:59 web.tp2.linux httpd[1463]: Server configured, listening on: port 80
```

- vérifier qu'il est configuré pour démarrer automatiquement

```
Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
```
 
- vérifier avec une commande curl localhost que vous joignez votre serveur web localement

```
[greg@web ~]$ curl localhost
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/
[...]
[greg@web ~]$ echo $?
0
```

### 2. Avancer vers la maîtrise du service

#### 🌞 Le service Apache...

- Affichez le contenu du fichier httpd.service qui contient la définition du service Apache

```
[greg@web ~]$ cat /usr/lib/systemd/system/httpd.service

[...]

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

#### 🌞 Déterminer sous quel utilisateur tourne le processus Apache

- Mettez en évidence la ligne dans le fichier de conf principal d'Apache (httpd.conf) qui définit quel user est utilisé

```
[greg@web ~]$ cat /etc/httpd/conf/httpd.conf

[...]

User apache
Group apache

[...]
```



```
[greg@web ~]$ ps -ef
UID          PID    PPID  C STIME TTY          TIME CMD
[...]
apache      1464    1463  0 15:01 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1465    1463  0 15:01 ?        00:00:02 /usr/sbin/httpd -DFOREGROUND
apache      1466    1463  0 15:01 ?        00:00:02 /usr/sbin/httpd -DFOREGROUND
apache      1467    1463  0 15:01 ?        00:00:02 /usr/sbin/httpd -DFOREGROUND
root        1708       2  0 15:02 ?        00:00:00 [kworker/u2:1-xfs-cil/dm-0]
root        1810       2  0 15:14 ?        00:00:00 [kworker/u2:0-events_unbound]
apache      1946    1463  0 16:24 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
[...]
```

- Vérifiez avec un ls -al que tout son contenu est accessible en lecture à l'utilisateur mentionné dans le fichier de conf

```
[greg@web ~]$ ls -al /usr/share/testpage/
total 12
drwxr-xr-x.  2 root root   24 Sep 29 16:38 .
drwxr-xr-x. 90 root root 4096 Sep 29 17:11 ..
-rw-r--r--.  1 root root 7621 Jun 11 17:23 index.html
```

Tous les utilisateurs ont les droits en lecture donc l'utilisateur mentionné y a accés.

#### 🌞 Changer l'utilisateur utilisé par Apache

- Créez le nouvel utilisateur

```
[greg@web ~]$ sudo adduser Apache
```

- Modifiez la configuration d'Apache pour qu'il utilise ce nouvel utilisateur

```
[greg@web ~]$ sudo vim /etc/httpd/conf/httpd.conf

[...]

User Apache
Group Apache

[...]
```

- Redémarrez Apache

```
[greg@web ~]$ sudo systemctl restart httpd
```

- Utilisez une commande ps pour vérifier que le changement a pris effet

```
[greg@web ~]$ ps -ef

[...]
Apache      1665    1662  0 23:04 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
Apache      1666    1662  0 23:04 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
Apache      1667    1662  0 23:04 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
Apache      1668    1662  0 23:04 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND

[...]
```

#### 🌞 Faites en sorte que Apache tourne sur un autre port

- Modifiez la configuration d'Apache pour lui demande d'écouter sur un autre port de votre choix

```
[greg@web ~]$ sudo vim /etc/httpd/conf/httpd.conf

[greg@web ~]$ cat /etc/httpd/conf/httpd.conf

ServerRoot "/etc/httpd"

Listen 1600

[...]
```

- Ouvrez un nouveau port firewall, et fermez l'ancien

```
[greg@web ~]$ sudo firewall-cmd --add-port=1600/tcp
success
[greg@web ~]$ sudo firewall-cmd --add-port=1600/tcp --permanent
success
```

```
[greg@web ~]$ sudo firewall-cmd --remove-port=80/tcp
success
[greg@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
```

- Redémarrez Apache

```
sudo systemctl restart httpd
```

- Prouvez avec une commande ss que Apache tourne bien sur le nouveau port choisi

```
[greg@web ~]$ sudo ss -tlpn
State      Recv-Q     Send-Q         Local Address:Port 

[...]

LISTEN     0          128                        *:1600                    *:*        users:(("httpd",pid=2150,fd=4),("httpd",pid=2149,fd=4),("httpd",pid=2148,fd=4),("httpd",pid=2145,fd=4))
```

- Vérifiez avec curl en local que vous pouvez joindre Apache sur le nouveau port

```
[greg@web ~]$ curl 10.102.1.11:1600
<!doctype html>
<html>
  <head>
[...]

[greg@web ~]$ echo $?
0
```

- Vérifiez avec votre navigateur que vous pouvez joindre le serveur sur le nouveau port

Le serveur est bien joignable depuis le navigateur sur le port 1600

## II. Une stack web plus avancée

### 2. Setup

### A. Serveur Web et NextCloud

#### 🌞 Install du serveur Web et de NextCloud sur web.tp2.linux

- Je veux dans le rendu toutes les commandes réalisées

```
- sudo dnf install epel-release
- sudo dnf update
- sudo dnf install
- https://rpms.remirepo.net/enterprise/remi-release-8.rpm
- dnf module list php
- sudo dnf module enable php:remi-7.4
- dnf module list php
- sudo dnf install httpd mariadb-server vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
- sudo systemctl enable httpd
- vim /etc/httpd/sites-available/linux.tp2.web
- ln -s /etc/httpd/sites-available/linux.tp2.web /etc/httpd/sites-enabled/
- sudo mkdir -p /var/www/sub-domains/linux.tp2.web/html
- ls -al /etc/localtime
- sudo wget https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
- unzip nextcloud-21.0.1.zip
- cd nextcloud
- cp -Rf * /var/www/sub-domains/linux.tp2.web/html/
- sudo chown -Rf apache.apache /var/www/sub-domains/linux.tp2.web/html
- sudo mv /var/www/sub-domains/linux.tp2.web/html/data /var/www/sub-domains/linux.tp2.web/
- systemctl restart httpd
- sudo firewall-cmd --add-port 80/tcp
- sudo firewall-cmd --add-port 80/tcp --permanent
```

### B. Base de données

#### 🌞 Install de MariaDB sur db.tp2.linux

```
   26  sudo dnf install mariadb-server
   27  sudo systemctl enable mariadb
   28  sudo systemctl start mariadb
   29  mysql_secure_installation
```

- Vous repérerez le port utilisé par MariaDB avec une commande ss exécutée sur db.tp2.linux

```
[greg@db ~]$ ss -alptn
State    Recv-Q   Send-Q      Local Address:Port       Peer Address:Port   Process
LISTEN   0        128               0.0.0.0:22              0.0.0.0:*
LISTEN   0        128                  [::]:22                 [::]:*
LISTEN   0        80                      *:3306                  *:*
```

#### 🌞 Préparation de la base pour NextCloud

```
[greg@db ~]$ sudo mysql -u root -p
[sudo] password for greg:
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 22
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```

#### 🌞 Exploration de la base de données

- Afin de tester le bon fonctionnement de la base de données, vous allez essayer de vous connecter, comme NextCloud le fera:

```
[greg@web html]$ mysql -u nextcloud -h 10.102.1.12 -pmeow

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.003 sec)

MariaDB [(none)]> USE nextcloud;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [nextcloud]> SHOW TABLES;
+-----------------------------+
| Tables_in_nextcloud         |
+-----------------------------+
| oc_accounts                 |
| oc_accounts_data            |
|                             |
|[...]                        |
|                             |
| oc_webauthn                 |
| oc_whats_new                |
+-----------------------------+
77 rows in set (0.004 sec)
```

- Trouver une commande qui permet de lister tous les utilisateurs de la base de données

```
MariaDB [(none)]> SELECT User, Host FROM mysql.user;
+-----------+-------------+
| User      | Host        |
+-----------+-------------+
| nextcloud | 10.102.1.11 |
| root      | 127.0.0.1   |
| root      | ::1         |
| root      | localhost   |
+-----------+-------------+
4 rows in set (0.000 sec)
```

### C. Finaliser l'installation de NextCloud

#### 🌞 sur votre PC

```
Se rendre dans un Bloc-Note en administrateur puis chercher le fichier: C:\Windows\System32\drivers\etc\hosts

y rentrer la ligne : 10.102.1.11 web.tp2.linux
```

| Machine         | IP            | Service                 | Port ouvert | IP autorisées                                   |
|-----------------|---------------|-------------------------|-------------|-------------------------------------------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 80/tcp      | toutes                                          |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | 3306/tcp    | 10.102.1.11                                     |

PS: J'ai voulu te mettre tous les fichiers une fois le tp terminé, mais du coup j'ai supprimé le premier fichier httpd.conf pour passer a la suite. Il n'y a donc que celui de la deuxieme partie dans les fichiers joins. 
